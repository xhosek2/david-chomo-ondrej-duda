from bs4 import BeautifulSoup
import requests
import pywebio.output as out
import pywebio.input as input
from pywebio import start_server,config
from pywebio.pin import *




class DuChoScrapper:
    def __init__(self, country):
        self.web = 'http://justwatch.com/' + country + '/'
        self.source =''
        self.search();
    def search(self):
        result = input.checkbox(label='Film alebo Séria', options=list)
        if len(result) == 2:
            out.put_error('Vyber si jedno alebo druhé', closable=False)
            raise Exception('Zadané boli obidve možnosti')
        elif result[0] == 'Film':
            result = 'film/'
            self.search_ok(result)
        elif result[0] == 'Seria':
            result = 'serial/'
            self.search_ok(result)
    def search_ok(self, result):
        meno = input.input('Zadaj meno filmu alebo seriálu')
        meno = meno.lower()
        meno = meno.replace(' ', '-')
        source = self.web + result + meno
        self.check_url(source)
        source = requests.get(source).text
        self.source = BeautifulSoup(source, 'lxml')
        if result == 'film/':
            self.name()
            self.streaming()
        else:
            self.name()
            self.streaming_series()
    def check_url(self,source):
        source = requests.get(source)
        if source.status_code != 200:
            out.put_error('Zlý názov filmu alebo seriálu',closable=False)
            raise Exception('Zlý názov filmu alebo seriálu')
    def name(self):
        name = self.source.find('div', class_ ="title-block")
        out.put_info(name.h1.text)
    def detail_info(self,stream,rent,buy):
        detail_info = self.source.find_all('div', class_="detail-infos__value")
        image = self.source.find('div', class_="title-poster title-poster--no-radius-bottom")
        final_image = image.source['data-srcset']
        final_image = final_image.split(',')
        out.put_row([
            out.put_column([
            out.put_code(f'Just Watch a IMDB: {detail_info[0].text}\n'
                     f'Žáner: {detail_info[1].text}\n'
                     f'Doba Spracovaní: {detail_info[2].text}\n'
                     f'Režisér: {detail_info[3].text}\n'
                     f'\nStream: \n{stream}\n'
                     f'Na prenájom: \n{rent}\n'
                     f'Koupit: \n{buy}\n'),None,
            ]),
            out.put_image(src= final_image[0],width='80%',height='80%'),
        ])
        pywebio.output.clear()
    def detail_info_series(self,stream):
        detail_info = self.source.find_all('div', class_="detail-infos__value")
        image = self.source.find('div', class_="title-poster title-poster--no-radius-bottom")
        final_image = image.source['data-srcset']
        final_image = final_image.split(',')
        out.put_row([
            out.put_column([
                out.put_code(f'Just Watch a IMDB: {detail_info[0].text}\n'
                             f'Žáner: {detail_info[1].text}\n'
                             f'Doba Spracovaní: {detail_info[2].text}\n'
                             f'\nStream: \n {stream}')
            ]),
            out.put_image(src=final_image[0], width='80%', height='80%'),
        ])
    def streaming(self):
        a = 0
        b = 0
        c = 0
        stream = ''
        buy = ''
        rent = ''
        try:
            streaming_stream = self.source.find('div', class_="price-comparison__grid__row price-comparison__grid__row--stream")
            streaming_stream_price = streaming_stream.find_all('div', class_="price-comparison__grid__row__price")
            streaming_stream_names = streaming_stream.find_all('picture', class_="provider-icon")
        except:
            pass
        else:
            streaming_stream = self.source.find('div',class_="price-comparison__grid__row price-comparison__grid__row--stream")
            streaming_stream_price = streaming_stream.find_all('div', class_="price-comparison__grid__row__price")
            streaming_stream_names = streaming_stream.find_all('picture', class_="provider-icon")
            for i in streaming_stream_names:
                stream = stream + (i.img['alt'] + ' ' + streaming_stream_price[a].text) + '\n'
                a += 1
        try:
            streaming_rent = self.source.find('div', class_="price-comparison__grid__row price-comparison__grid__row--rent")
            streaming_rent_price = streaming_rent.find_all('div', class_="price-comparison__grid__row__price")
            streaming_rent_names = streaming_rent.find_all('picture', class_="provider-icon")
        except:
            pass
        else:
            streaming_rent = self.source.find('div',class_="price-comparison__grid__row price-comparison__grid__row--rent")
            streaming_rent_price = streaming_rent.find_all('div', class_="price-comparison__grid__row__price")
            streaming_rent_names = streaming_rent.find_all('picture', class_="provider-icon")
            for i in streaming_rent_names:
                rent = rent + (i.img['alt'] + ' ' + streaming_rent_price[a].text) + '\n'
                b += 1
        try:
            streaming_buy = self.source.find('div',class_="price-comparison__grid__row price-comparison__grid__row--buy")
            streaming_buy_price = streaming_buy.find_all('div', class_="price-comparison__grid__row__price")
            streaming_buy_names = streaming_buy.find_all('picture', class_="provider-icon")
        except:
            pass
        else:
            streaming_buy = self.source.find('div',class_="price-comparison__grid__row price-comparison__grid__row--buy")
            streaming_buy_price = streaming_buy.find_all('div', class_="price-comparison__grid__row__price")
            streaming_buy_names = streaming_buy.find_all('picture', class_="provider-icon")
            for i in streaming_buy_names:
                buy = buy + (i.img['alt'] + ' ' + streaming_buy_price[a].text) + '\n'
                c += 1
        self.detail_info(stream,rent,buy)
    def streaming_series(self):
        a = 0
        stream = ''
        try:
            streaming_stream = self.source.find('div',class_="price-comparison__grid__row price-comparison__grid__row--stream")
            streaming_stream_price = streaming_stream.find_all('div', class_="price-comparison__grid__row__price")
            streaming_stream_names = streaming_stream.find_all('picture', class_="provider-icon")
        except:
            pass
        else:
            streaming_stream = self.source.find('div',class_="price-comparison__grid__row price-comparison__grid__row--stream")
            streaming_stream_price = streaming_stream.find_all('div', class_="price-comparison__grid__row__price")
            streaming_stream_names = streaming_stream.find_all('picture', class_="provider-icon")
            if not streaming_stream_names:
                stream = ('Nie je ešte k dispozícií')
                self.detail_info_series(stream)
                return None
            for i in streaming_stream_names:
                stream = stream + (i.img['alt'] + ' ' + streaming_stream_price[a].text) + '\n'
                a += 1
        self.detail_info_series(stream)
    def __del__(self):
        pass
if __name__ == '__main__':
    list = ('Film', 'Seria')
    ok = DuChoScrapper('cz')